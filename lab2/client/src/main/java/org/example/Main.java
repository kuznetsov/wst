package org.example;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.ClientResponse;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class Main {
    private static final String URL =
            "http://localhost:8080/animals/";

    public static void main(String[] args) {

        Client client = Client.create();
        // Get all animals from service
        printList(getAllAnimals(client));
        // Get specified animal by id (exist)
        System.out.println("Founded animal by id: " + getAnimalById(client, 11L));
        // Get specified animal by id (NOT exist)
        System.out.println("NOT Founded animal by id (-1):" + getAnimalById(client, -1L));

        // Create animal
        Long newCatId = createAnimal(client, "cat", false, "m", 3, "white");
        System.out.println("New cat created with id: " + newCatId);
        Long newDogId = createAnimal(client, "dog", true, "f", 5, "black");
        System.out.println("New dog created with id: " + newDogId);

        // Show created DOG
        System.out.println("Founded dog by id: " + getAnimalById(client, newDogId));

        // Animal updating
        String result = updateAnimal(client, newDogId, "horse", false, "m", 20, "red");
        System.out.println("Result of animal updating: " + result);
        String badResult = updateAnimal(client, 100000L, "penguin", false, "m", 20, "red");
        System.out.println("Result of false animal updating: " + badResult);

        // Animal deleting
        System.out.println("Delete animal by id result code: " + deleteAnimal(client, newDogId));
        // animal removal check
        System.out.println("NOT Founded animal by deleted id:" + getAnimalById(client, newDogId));
    }

    private static List<Animal> getAllAnimals(Client client)
    {
        WebResource webResource = client.resource(URL);

        ClientResponse response =
                webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if (response.getStatus() != 200) {
            throw new IllegalStateException("Request failed");
        }

        GenericType<List<Animal>> type = new GenericType<List<Animal>>() {};
        return response.getEntity(type);
    }

    private static Animal getAnimalById(Client client, Long id)
    {
        WebResource webResource = client.resource(URL);
        webResource = webResource.path(String.valueOf(id));

        ClientResponse response =
                webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if (response.getStatus() != 200) {
            System.out.println("getAnimalById: Animal not found! It's id " + id);
            return null;
        }

        GenericType<Animal> type = new GenericType<Animal>() {};
        return response.getEntity(type);
    }

    private static Long createAnimal(Client client,
                                     String type,
                                     boolean issterilized,
                                     String gender,
                                     int age,
                                     String color)
    {
        WebResource webResource = client.resource(URL);
        webResource = webResource.queryParam("type", type);
        webResource = webResource.queryParam("issterilized", String.valueOf(issterilized));
        webResource = webResource.queryParam("gender", gender);
        webResource = webResource.queryParam("age", String.valueOf(age));
        webResource = webResource.queryParam("color", color);

        ClientResponse response =
                webResource.accept(MediaType.APPLICATION_JSON).post(ClientResponse.class);
        if (response.getStatus() != 200) {
            return -1L; // create failed
        }

        GenericType<String> resType = new GenericType<String>() {};
        return Long.decode(response.getEntity(resType));
    }

    private static String updateAnimal(Client client,
                                     Long id,
                                     String type,
                                     boolean issterilized,
                                     String gender,
                                     int age,
                                     String color)
    {
        WebResource webResource = client.resource(URL);
        webResource = webResource.path(String.valueOf(id));
        webResource = webResource.queryParam("type", type);
        webResource = webResource.queryParam("issterilized", String.valueOf(issterilized));
        webResource = webResource.queryParam("gender", gender);
        webResource = webResource.queryParam("age", String.valueOf(age));
        webResource = webResource.queryParam("color", color);

        ClientResponse response =
                webResource.accept(MediaType.APPLICATION_JSON).put(ClientResponse.class);

        if (response.getStatus() != 200) {
            return String.format("Animal was not updated with id: %d \n", id);
        }

        return String.format("Animal with id %d was updated!\n", id);
    }

    private static Response.Status deleteAnimal(Client client, Long id)
    {
        WebResource webResource = client.resource(URL);
        webResource = webResource.path(String.valueOf(id));

        ClientResponse response =
                webResource.accept(MediaType.APPLICATION_JSON).delete(ClientResponse.class);

        if (response.getStatus() != 204) {
            return Response.Status.BAD_REQUEST; // delete animal request failed
        }

        return Response.Status.NO_CONTENT;
    }

    private static <T> void printList(List<T> smths) {
        System.out.println("Whole animal list: ");
        for (T smth : smths) {
            System.out.println(smth);
        }
        System.out.println("\n");
    }
}