package org.example;

import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.ClassNamesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import org.glassfish.grizzly.http.server.HttpServer;

import java.io.IOException;
import java.net.URI;

public class Main {
    public static void main(String[] args) {

        URI baseUri = URI.create("http://localhost:8080/");

        HttpServer server = null;
        try {
            ResourceConfig resourceConfig = new
                    ClassNamesResourceConfig(AnimalResource.class);
            server = GrizzlyServerFactory.createHttpServer(baseUri,
                    resourceConfig);
            server.start();
            System.in.read();
            stopServer(server);
        } catch (IOException e) {
            e.printStackTrace();
            stopServer(server);
        }
    }

    private static void stopServer(HttpServer server) {
        if (server != null) {
            server.stop();
        }
    }
}