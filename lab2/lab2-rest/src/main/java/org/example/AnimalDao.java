package org.example;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AnimalDao {

    public void deleteAnimal(Long id) throws SQLException {
        try (Connection connection = Database.getConnection()) {

            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    "DELETE FROM Animal WHERE id=?",
                    Statement.RETURN_GENERATED_KEYS)) // Return id
            {
                preparedStatement.setLong(1, id);

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0) {
                    throw new SQLException("Deleting animal failed, no rows affected.");
                }
            }
        }
    }

    public void updateAnimal(Long oldId, String type, boolean isSterilized, char gender, int age, String color) throws SQLException {
        try (Connection connection = Database.getConnection()) {

            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE Animal SET type=?, issterilized=?, gender=?, age=?, color=? WHERE id=?",
                    Statement.RETURN_GENERATED_KEYS)) // Return id
            {
                preparedStatement.setString(1, type);
                preparedStatement.setBoolean(2, isSterilized);
                preparedStatement.setString(3, String.valueOf(gender));
                preparedStatement.setInt(4, age);
                preparedStatement.setString(5, color);
                preparedStatement.setLong(6, oldId);

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0) {
                    System.out.printf("Animal with id %d was not updated!\n", oldId);
                    throw new SQLException("Updating animal failed, no rows affected.");
                }
            }
        }
    }

    public Long createAnimal(String type, boolean isSterilized, char gender, int age, String color) {

        try (Connection connection = Database.getConnection()) {

            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO Animal (type, issterilized, gender, age, color) VALUES (?, ?, ?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS)) // Return id
            {
                preparedStatement.setString(1, type);
                preparedStatement.setBoolean(2, isSterilized);
                preparedStatement.setString(3, String.valueOf(gender));
                preparedStatement.setInt(4, age);
                preparedStatement.setString(5, color);

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0) {
                    throw new SQLException("Creating animal failed, no rows affected.");
                }
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getLong(1);
                    }
                    else {
                        throw new SQLException("Creating animal failed, no ID obtained.");
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1L;
    }

    public List<Animal> getAllAnimals() {
        List<Animal> animals = new ArrayList<>();
        try (Connection connection = Database.getConnection()) {

            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Animal")) {
                ResultSet resultSet = preparedStatement.executeQuery();


                // Process the results and populate the Animal objects
                while (resultSet.next()) {
                    Animal animal = new Animal(
                            resultSet.getLong("id"),
                            resultSet.getString("type"),
                            resultSet.getBoolean("isSterilized"),
                            resultSet.getString("gender").charAt(0),
                            resultSet.getInt("age"),
                            resultSet.getString("color")
                    );
                    animals.add(animal);
                }
                return animals;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return animals;
    }

    public Animal getAnimalById(Long id) {
        try (Connection connection = Database.getConnection()) {

            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Animal WHERE id = " + id)) {
                ResultSet resultSet = preparedStatement.executeQuery();

                if (resultSet.wasNull()) {
                    return null;
                }

                resultSet.next();

                return new Animal(
                        resultSet.getLong("id"),
                        resultSet.getString("type"),
                        resultSet.getBoolean("isSterilized"),
                        resultSet.getString("gender").charAt(0),
                        resultSet.getInt("age"),
                        resultSet.getString("color")
                );
            }
        } catch (SQLException e) {
            System.out.printf("Animal with id %d was not found!\n", id);
        }
        return null;
    }

    public List<Animal> getAnimalByCharacteristics(
            String type,
            boolean isSterilized,
            char gender,
            int minAge,
            int maxAge,
            String color
    ) {
        List<Animal> animals = new ArrayList<>();
        try (Connection connection = Database.getConnection()){

            try (PreparedStatement preparedStatement = connection.prepareStatement(searchQuery(type, isSterilized, gender, minAge, maxAge, color))) {
                int parameterIndex = 1;

                // Set parameters based on the conditions
                if (type != null && !type.isEmpty()) {
                    preparedStatement.setString(parameterIndex++, type);
                }

                if (gender != '\0') {
                    preparedStatement.setString(parameterIndex++, String.valueOf(gender));
                }
                if (minAge >= 0 && maxAge >= 0) {
                    preparedStatement.setInt(parameterIndex++, minAge);
                    preparedStatement.setInt(parameterIndex++, maxAge);
                } else if (minAge >= 0) {
                    preparedStatement.setInt(parameterIndex++, minAge);
                } else if (maxAge >= 0) {
                    preparedStatement.setInt(parameterIndex++, maxAge);
                }
                if (color != null && !color.isEmpty()) {
                    preparedStatement.setString(parameterIndex, color);
                }

                // Execute the query
                ResultSet resultSet = preparedStatement.executeQuery();

                // Process the results and populate the Animal objects
                while (resultSet.next()) {
                    Animal animal = new Animal(
                            resultSet.getLong("id"),
                            resultSet.getString("type"),
                            resultSet.getBoolean("isSterilized"),
                            resultSet.getString("gender").charAt(0),
                            resultSet.getInt("age"),
                            resultSet.getString("color")
                    );
                    animals.add(animal);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(AnimalDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return animals;
    }

    /**
     * Builds animal query with passed parameters.
     * @param type Animal type.
     * @param isSterilized Is animal sterilized.
     * @param gender Animal gender.
     * @param minAge Animal min age.
     * @param maxAge Animal max age.
     * @param color Animal color.
     *
     * @return SQL query as string with placeholders.
     */
    private String searchQuery(String type,
                               boolean isSterilized,
                               char gender,
                               int minAge,
                               int maxAge,
                               String color)
    {
        StringBuilder query = new StringBuilder("SELECT * FROM Animal WHERE 1=1");
        // Check if parameters are specified and append conditions to the SQL query accordingly
        if (type != null && !type.isEmpty()) {
            query.append(" AND type = ?");
        }

        query.append(" AND isSterilized = ").append(isSterilized);

        if (gender != '\0') {
            query.append(" AND gender = ?");
        }
        if (minAge >= 0 && maxAge >= 0) {
            query.append(" AND age BETWEEN ? AND ?");
        } else if (minAge >= 0) {
            query.append(" AND age >= ?");
        } else if (maxAge >= 0) {
            query.append(" AND age <= ?");
        }
        if (color != null && !color.isEmpty()) {
            query.append(" AND color = ?");
        }

        return query.toString();
    }
}
