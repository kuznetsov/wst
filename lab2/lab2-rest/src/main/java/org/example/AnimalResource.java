package org.example;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.List;

@Path("/animals")
@Produces({MediaType.APPLICATION_JSON})
public class AnimalResource {

    private AnimalDao animalDao = new AnimalDao();

    @GET
    @Path("/{id}")
    public Animal getAnimalById(@PathParam("id") Long id)
    {
        return animalDao.getAnimalById(id);
    }

    @GET
    public List<Animal> getAllAnimals()
    {
        return animalDao.getAllAnimals();
    }

    @POST
    public String createAnimal(
            @QueryParam("type") String type,
            @QueryParam("issterilized") boolean issterilized,
            @QueryParam("gender") String gender,
            @QueryParam("age") int age,
            @QueryParam("color") String color)
    {
        return String.valueOf(animalDao.createAnimal(type, issterilized, gender.charAt(0), age, color));
    }

    @PUT
    @Path("/{id}")
    public Response updateAnimal(
            @PathParam("id") Long id,
            @QueryParam("type") String type,
            @QueryParam("issterilized") boolean issterilized,
            @QueryParam("gender") String gender,
            @QueryParam("age") int age,
            @QueryParam("color") String color
    ) {
        try {
            animalDao.updateAnimal(id, type, issterilized, gender.charAt(0), age, color);
            return Response.ok().build();
        } catch (SQLException e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response deleteAnimal(@PathParam("id") Long id) {
        try {
            animalDao.deleteAnimal(id);
            return Response.noContent().build();
        } catch (SQLException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
