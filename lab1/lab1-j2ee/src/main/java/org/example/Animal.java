package org.example;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Animal {
    private Long id;
    private String type;
    private boolean isSterilized;
    private int gender;
    private int age;
    private String color;

    public Animal() { }

    public Animal(Long id,
                  String type,
                  boolean isSterilized,
                  char gender,
                  int age,
                  String color) {
        this.id = id;
        this.type = type;
        this.isSterilized = isSterilized;
        this.gender = gender;
        this.age = age;
        this.color = color;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlElement
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @XmlElement
    public boolean getIsSterilized() {
        return isSterilized;
    }

    public void setIsSterilized(boolean isSterilized) {
        this.isSterilized = isSterilized;
    }

    @XmlElement
    public int getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    @XmlElement
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @XmlElement
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return String.format("Animal { type: %s, sterilized: %b, gender: %c, age:%d, color:%s }",
                type,
                isSterilized,
                gender,
                age,
                color);
    }
}
