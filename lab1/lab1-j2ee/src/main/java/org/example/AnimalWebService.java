package org.example;

import com.sun.istack.logging.Logger;
import jakarta.annotation.PostConstruct;
import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

@WebService(serviceName = "AnimalService")
public class AnimalWebService {
    private DataSource dataSource;

    public AnimalWebService() {
        try {
            InitialContext initialContext = new InitialContext();
            dataSource = (DataSource) initialContext.lookup("jdbc/web-tech");
            System.err.println("DataSource is created: " + (dataSource != null));
        } catch (Exception e) {
            System.err.println("Init exception: " + e);
        }
    }

    @WebMethod(operationName = "animalByCharacteristics")
    public List<Animal> getAnimalsByCharacteristics(
            @WebParam(name = "arg0") String type,
            @WebParam(name = "arg1") boolean isSterilized,
            @WebParam(name = "arg2") char gender,
            @WebParam(name = "arg3") int minAge,
            @WebParam(name = "arg4") int maxAge,
            @WebParam(name = "arg5") String color
    ) {
        Connection connection = getConnection();
        AnimalDao dao = new AnimalDao(connection);

        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM animal");
            ResultSet resultSet = statement.executeQuery();

            System.out.println("Params: ");
            System.out.println("type: " + type);
            System.out.println("isSterilized: " + isSterilized);
            System.out.println("gender: " + gender);
            System.out.println("minAge: " + minAge);
            System.out.println("maxAge: " + maxAge);
            System.out.println("color: " + color);

            // Process the results and populate the Animal objects
            while (resultSet.next()) {
                Animal animal = new Animal(
                        resultSet.getLong("id"),
                        resultSet.getString("type"),
                        resultSet.getBoolean("isSterilized"),
                        resultSet.getString("gender").charAt(0),
                        resultSet.getInt("age"),
                        resultSet.getString("color")
                );
                System.out.println("get animal " + animal);
            }

            return dao.getAnimalByCharacteristics(type, isSterilized, gender, minAge, maxAge, color);
        } catch (SQLException e) {
            Logger.getLogger("AnimalWebService", AnimalWebService.class).log(Level.SEVERE, null,
                    "Error while getting animal!");
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return new ArrayList<>();
    }

    private Connection getConnection() {
        Connection connection;
        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            System.out.println("Connection get exception!");
            throw new RuntimeException(e);
        }
        System.out.println("Getting connection " + connection);
        return connection;
    }
}
