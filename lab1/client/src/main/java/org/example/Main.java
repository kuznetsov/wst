package org.example;

import org.example.service.Animal;
import org.example.service.AnimalService;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class Main {
    public static void main(String[] args) throws MalformedURLException {
        URL url = new URL("http://localhost:8081/lab1j2ee/animalService?wsdl"); // from local standalone
        // URL url = new URL("http://localhost:8080/lab1j2ee/animalService?wsdl"); // from Glassfish
        AnimalService animalService = new AnimalService(url);

        List<Animal> anySterilizedMale1to5White =
                animalService.getAnimalWebServicePort().animalByCharacteristics(
                        null, true, 'm', 1, 5, "white"
                );

        System.out.println("Select ANY STERILIZED MALE FROM 1 TO 5 AGE WITH WHITE COLOR: ");

        for (Animal animal : anySterilizedMale1to5White) {
            System.out.println(animal.toString());
        }
        System.out.println("Founded animal: " + anySterilizedMale1to5White.size());

        List<Animal> catSterilizedFemale0to10 =
                animalService.getAnimalWebServicePort().animalByCharacteristics(
                        "cat", false, 'f', 0, 10, null
                );

        System.out.println("\nSelect CAT NOT STERILIZED FEMALE FROM 0 TO 10 AGE: ");

        for (Animal animal : catSterilizedFemale0to10) {
            System.out.println(animal.toString());
        }
        System.out.println("Founded animal: " + catSterilizedFemale0to10.size());


        List<Animal> femaleDogsSterilized =
                animalService.getAnimalWebServicePort().animalByCharacteristics(
                        "dog", true, 'm', 0, 10, null
                );

        System.out.println("\nSelect DOGS STERILIZED FEMALE FROM 0 TO 10 AGE: ");

        for (Animal animal : femaleDogsSterilized) {
            System.out.println(animal.toString());
        }
        System.out.println("Founded animal: " + femaleDogsSterilized.size());

        List<Animal> greenAnimals =
                animalService.getAnimalWebServicePort().animalByCharacteristics(
                        null, false, '\0', -1, -1, "green"
                );

        System.out.println("\nSelect NOT STERILIZED GREEN ANIMALS: ");

        for (Animal animal : greenAnimals) {
            System.out.println(animal.toString());
        }
        System.out.println("Founded animal: " + greenAnimals.size());
    }
}