package org.example;


import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(serviceName = "AnimalService")
public class AnimalWebService {

    @WebMethod(operationName = "animalByCharacteristics")
    public List<Animal> getAnimalByCharacteristics(
           String type,
            boolean isSterilized,
            char gender,
            int minAge,
            int maxAge, String color
    ) {
        AnimalDao dao = new AnimalDao();
        return dao.getAnimalByCharacteristics(type, isSterilized, gender, minAge, maxAge, color);
    }
}
