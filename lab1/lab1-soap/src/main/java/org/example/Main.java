package org.example;

import javax.xml.ws.Endpoint;

public class Main {
    public static void main(String[] args) {
        String url = "http://0.0.0.0:8081/lab1j2ee/animalService";
        Endpoint.publish(url, new AnimalWebService());
    }
}