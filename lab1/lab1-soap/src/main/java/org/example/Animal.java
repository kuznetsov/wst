package org.example;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "animal", propOrder = {
        "age",
        "color",
        "gender",
        "id",
        "isSterilized",
        "type"
})
public class Animal {
    @XmlElement(namespace = "http://example.org/")
    private Long id;
    @XmlElement(namespace = "http://example.org/") private String type;
    @XmlElement(namespace = "http://example.org/") private boolean isSterilized;
    @XmlElement(namespace = "http://example.org/") private char gender;
    @XmlElement(namespace = "http://example.org/") private int age;
    @XmlElement(namespace = "http://example.org/") private String color;

    public Animal() { }

    public Animal(Long id,
                  String type,
                  boolean isSterilized,
                  char gender,
                  int age,
                  String color) {
        this.id = id;
        this.type = type;
        this.isSterilized = isSterilized;
        this.gender = gender;
        this.age = age;
        this.color = color;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean getIsSterilized() {
        return isSterilized;
    }

    public void setIsSterilized(boolean isSterilized) {
        this.isSterilized = isSterilized;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return String.format("Animal { type: %s, sterilized: %b, gender: %c, age:%d, color:%s }",
                type,
                isSterilized,
                gender,
                age,
                color);
    }
}
