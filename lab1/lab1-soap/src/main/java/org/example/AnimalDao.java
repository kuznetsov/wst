package org.example;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AnimalDao {

    public List<Animal> getAnimalByCharacteristics(
            String type,
            boolean isSterilized,
            char gender,
            int minAge,
            int maxAge,
            String color
    ) {
        List<Animal> animals = new ArrayList<>();
        try (Connection connection = Database.getConnection()){

            try (PreparedStatement preparedStatement = connection.prepareStatement(searchQuery(type, isSterilized, gender, minAge, maxAge, color))) {
                int parameterIndex = 1;

                // Set parameters based on the conditions
                if (type != null && !type.isEmpty()) {
                    preparedStatement.setString(parameterIndex++, type);
                }

                if (gender != '\0') {
                    preparedStatement.setString(parameterIndex++, String.valueOf(gender));
                }
                if (minAge >= 0 && maxAge >= 0) {
                    preparedStatement.setInt(parameterIndex++, minAge);
                    preparedStatement.setInt(parameterIndex++, maxAge);
                } else if (minAge >= 0) {
                    preparedStatement.setInt(parameterIndex++, minAge);
                } else if (maxAge >= 0) {
                    preparedStatement.setInt(parameterIndex++, maxAge);
                }
                if (color != null && !color.isEmpty()) {
                    preparedStatement.setString(parameterIndex, color);
                }

                // Execute the query
                ResultSet resultSet = preparedStatement.executeQuery();

                // Process the results and populate the Animal objects
                while (resultSet.next()) {
                    Animal animal = new Animal(
                            resultSet.getLong("id"),
                            resultSet.getString("type"),
                            resultSet.getBoolean("isSterilized"),
                            resultSet.getString("gender").charAt(0),
                            resultSet.getInt("age"),
                            resultSet.getString("color")
                    );
                    animals.add(animal);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(AnimalDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return animals;
    }

    /**
     * Builds animal query with passed parameters.
     * @param type Animal type.
     * @param isSterilized Is animal sterilized.
     * @param gender Animal gender.
     * @param minAge Animal min age.
     * @param maxAge Animal max age.
     * @param color Animal color.
     *
     * @return SQL query as string with placeholders.
     */
    private String searchQuery(String type,
                               boolean isSterilized,
                               char gender,
                               int minAge,
                               int maxAge,
                               String color)
    {
        StringBuilder query = new StringBuilder("SELECT * FROM Animal WHERE 1=1");
        // Check if parameters are specified and append conditions to the SQL query accordingly
        if (type != null && !type.isEmpty()) {
            query.append(" AND type = ?");
        }

        query.append(" AND isSterilized = ").append(isSterilized);

        if (gender != '\0') {
            query.append(" AND gender = ?");
        }
        if (minAge >= 0 && maxAge >= 0) {
            query.append(" AND age BETWEEN ? AND ?");
        } else if (minAge >= 0) {
            query.append(" AND age >= ?");
        } else if (maxAge >= 0) {
            query.append(" AND age <= ?");
        }
        if (color != null && !color.isEmpty()) {
            query.append(" AND color = ?");
        }

        return query.toString();
    }
}
